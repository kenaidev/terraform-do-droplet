variable "digitalocean_token" {}

#Configure provider 


terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.8.0"
    }
  }
  backend "local" {
    path = "statePath/terraform.tfstate"
  }
}

provider "digitalocean" {
    token = "${var.digitalocean_token}"
}
